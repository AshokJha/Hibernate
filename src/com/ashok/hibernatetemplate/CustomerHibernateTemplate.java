package com.ashok.hibernatetemplate;

import java.io.Serializable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ashok.hibernateutil.HibernateUtil;

public class CustomerHibernateTemplate {
	
	public static Object SaveObject(Object obj){
		Object id = null;
		
		try{
			SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			id =session.save(obj);
			transaction.commit();
			session.close();
			
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
		return id;
		
	}
	public static void updateObject(Object obj){
		try{
			SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.update(obj);
			transaction.commit();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public static void deleteObject(Class cls,Serializable s){
		try{
			SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			Object obj = session.load(cls, s);
			session.delete(obj);
			transaction.commit();
			session.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public static Object loadObject(Class cls,Serializable s){
		Object object = null;
		try{
			SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			 object = session.load(cls, s);
			transaction.commit();
			session.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return object;
		
	}
	
	

}
