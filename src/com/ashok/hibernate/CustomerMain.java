package com.ashok.hibernate;

import com.ashok.hibernatedao.CustomerDAO;
import com.ashok.hibernatedao.CustomerDAOFactory;
import com.ashok.hibernateto.CustomerTO;

public class CustomerMain {
	public static void main(String[] args) {
		CustomerDAO cdao = CustomerDAOFactory.getCustomerDao();
		
		CustomerTO cto = new CustomerTO(2,"as","aa","aaa","aaaa");
		cdao.addCustomer(cto);
		
		CustomerTO cl =cdao.getCustomerById(1);
		System.out.println(cl.getId()+", "+cl.getName()+", "+cl.getEmail()+" ,"+cl.getCity()+" ,"+cl.getPhone());
		
	}

}
