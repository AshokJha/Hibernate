package com.ashok.hibernateutil;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	static SessionFactory sessionFactory;

	static {
		Configuration configuration = new Configuration();
		configuration = configuration.configure();
		sessionFactory = configuration.buildSessionFactory();

	}

	public static SessionFactory getSessionFactory() {

		return sessionFactory;

	}

}
