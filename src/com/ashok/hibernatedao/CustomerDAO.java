package com.ashok.hibernatedao;

import com.ashok.hibernateto.CustomerTO;

public interface CustomerDAO {
	public int addCustomer(CustomerTO csut);
	public void updateCustomer(CustomerTO csut);
	public void deleteCustomer(int id);
	public CustomerTO getCustomerById(int id);
	
	

}
