package com.ashok.hibernatedao;

import com.ashok.hibernate.Customer;
import com.ashok.hibernatetemplate.CustomerHibernateTemplate;
import com.ashok.hibernateto.CustomerTO;

public class HibernateCustomerDAO implements CustomerDAO{

	@Override
	public int addCustomer(CustomerTO csut) {
		Customer customer = new Customer(csut.getName(),csut.getEmail(),csut.getPhone(),csut.getCity());
		Integer it =(Integer)CustomerHibernateTemplate.SaveObject(customer);
		
		return it.intValue();
	}

	@Override
	public void updateCustomer(CustomerTO csut) {
		Customer customer = new Customer(csut.getName(),csut.getEmail(),csut.getPhone(),csut.getCity());
		CustomerHibernateTemplate.updateObject(customer);
	}

	@Override
	public void deleteCustomer(int id) {
		CustomerHibernateTemplate.deleteObject(Customer.class, id);
	}

	@Override
	public CustomerTO getCustomerById(int id) {
		Customer cust =(Customer)CustomerHibernateTemplate.loadObject(Customer.class, id);
		CustomerTO customer = new CustomerTO(cust.getId(),cust.getName(),cust.getEmail(),cust.getPhone(),cust.getCity());
		return customer;
	}

}
