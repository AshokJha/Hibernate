package com.ashok.hibernatedao;

public class CustomerDAOFactory {
	static CustomerDAO customerDAO;

	static {
		customerDAO = new HibernateCustomerDAO();
	}

	public static CustomerDAO getCustomerDao() {

		return customerDAO;

	}

}
